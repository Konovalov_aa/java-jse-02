package ru.konovalov.tm;

import ru.konovalov.tm.constant.TerminalConst;

import java.util.Arrays;

import static ru.konovalov.tm.constant.TerminalConst.*;

public class Main {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }
    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        if (CMD_HELP.equals(param)) displayHelp();
        if (CMD_VERSION.equals(param)) displayVersion();
        if (CMD_ABOUT.equals(param)) displayAbout();
    }
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }
    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }
    private static void displayVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }
    private static void displayAbout() {
        System.out.println("Konovalov Aleksandr");
        System.out.println("konovalov_aa@nlmk.com");
        System.exit(0);
    }

}
